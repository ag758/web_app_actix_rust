# web_app_actix_rust

# Containerize a Rust Actix/Axum Microservice - Lotto Generator

## Video for the project: 
https://www.youtube.com/watch?v=SR8F8hUhePw

## Description
This project containerizes a Rust Actix/Axum web application which is a rust microservice that generates random prime numbers based on the specified number of digits. Additionally, it demonstrates how to deploy the microservice to AWS AppRunner, making it accessible via the cloud.

## Usage

Try the following to run the app:

```
# local
cd lotto_actix # or lotto_randomizer
cargo build
cargo run
curl "localhost:8080/5" # 3000 for axum

# docker
make build
make rundocker
curl "localhost:8080/5" # 3000 for axum

```


## Setup Instructions

1. Make a directory for the project and navigate inside it.
```
$ cargo new your_project
$ cd your_project
```
2. Create the code for main.rs
```
use actix_web::{web, App, HttpServer, Responder};
use rand::Rng;

fn is_prime(num: u64) -> bool {
    if num <= 1 {
        return false;
    }
    for i in 2..=(num as f64).sqrt() as u64 {
        if num % i == 0 {
            return false;
        }
    }
    true
}

async fn generate_random_prime() -> impl Responder {
    let mut rng = rand::thread_rng();
    let mut num = rng.gen_range(1000..10000);

    while !is_prime(num) {
        num = rng.gen_range(1000..10000);
    }

    format!("Random Prime Number: {}", num)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(generate_random_prime))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}

```

The code for the Docker file: 
```
FROM rust:latest as builder
ENV APP rust_app
WORKDIR /usr/src/$APP
COPY . .
RUN cargo install --path .

# use the testing debian
FROM debian:testing
RUN apt-get update && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/$APP /usr/local/bin/$APP
#export this actix web service to port 8080 and 0.0.0.0
EXPOSE 8080
CMD ["rust_app"]

```
3. Build the project. Run the project in the terminal. 
```
cargo build
cargo run
```

4. Check Service Root Endpoint:

In a separate terminal session, execute the following command to test the root endpoint of your microservice:

```
curl 'localhost:8080'
```


5. Terminate your original terminal session running the "cargo run" command with by pressing control+c. Now, Proceed to building your docker. The dockerfile used for the project is derived from the MLOPS [template](https://github.com/nogibjj/rust-mlops-template/blob/main/webdocker/Dockerfile) from [Noah Gift](https://noahgift.com/). 
```
docker build -t any_lowercase_name_for_your_image .
```


![Screenshot of Docker run](Screenshot_2024-02-23_234126.png)
